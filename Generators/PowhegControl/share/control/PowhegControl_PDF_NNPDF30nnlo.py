# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
PowhegConfig.PDF = range(261000, 261101)